import React from 'react';
import {AsyncStorage} from 'react-native';
import getTheme from './native-base-theme/components';  
import { StyleSheet, Text, View } from 'react-native';
import { StyleProvider } from 'native-base';
import Expo from 'expo';
import firebase from './app/utils/Firebase';
import material from './native-base-theme/variables/material';
import Router from './app/utils/Router';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: true, isLoggedIn:false ,neightborhood:null,};
  }
  async componentWillMount() {
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      'Ionicons': require('@expo/vector-icons/fonts/Ionicons.ttf'),
      'Montserrat': require('./assets/fonts/Montserrat-Regular.otf')
    });
    try{
      let uid = await AsyncStorage.getItem('uid');
      if(uid){
        this.setState({isLoggedIn:true})
      }
      let neightborhood = await AsyncStorage.getItem('neightborhood');
      if(neightborhood){
        this.setState({neightborhood:true})
      }
    }
    catch(error){
      console.log('error getting info: ', error);
    }
    this.setState({isLoading:false})
  }
  
  render() {
    return (
      <StyleProvider  style={getTheme(material)}>   
      {
        this.state.isLoading?
          <Expo.AppLoading />
        :
        <Router isLoggedIn={ this.state.isLoggedIn } neightborhood={this.state.neightborhood} />
      }
      </StyleProvider> 
    );
  }
}
