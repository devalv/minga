import React from 'react';
import { StyleSheet, View, Image,ActivityIndicator,Clipboard,Share,Alert } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab,Picker, Button, Left, Right, Item,Input,Textarea,Spinner,Body, Icon, Text ,Form} from 'native-base';
import logo from '../assets/logo.png';
import {setItem,getItem,uploadMinga} from '../utils/Controller';
import Exponent, { Constants,Location, ImagePicker, Camera, Permissions, registerRootComponent } from 'expo';
import { ActionSheetProvider, connectActionSheet } from '@expo/react-native-action-sheet';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import DateTimePicker,{TimePicker} from 'react-native-modal-datetime-picker';

export default class First extends React.Component {
  constructor(props){
    super(props);
    this.state={
      location:null,
      isDateTimePickerVisible: false,
      mingaDate:null,
      title:'',
      description:'',
      selectCategory:'Limpieza',
      categories:['Lost Pet','Seguridad','Limpieza','Iluminación']
    };
  }
  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
  _handleDatePicked = (date) => {
    console.log('A date has been picked: ', date);
    let mingaDate = this.formatDate(date)
    mingaDate += '   09:00';
    this.setState({mingaDate});
    this._hideDateTimePicker();
  };
  
  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        error: 'Permission to access location was denied',
      });
    }
    let location = await Location.getCurrentPositionAsync({});
    let currentLocation = [location.coords.latitude, location.coords.longitude];
    this.setState({ location:currentLocation });
  };
  startOver(){
    this._getLocationAsync();
  }
  selectCategory = (value) =>{
    console.log('value: ', value);
    Actions.index();
    this.setState({
      selectCategory: value
    });
  }
  doMinga(){
    console.log('here');
    uploadMinga(data).then(value=>{
      console.log('value: ', value);
      Actions.index();
    }).catch(error=>{
      console.log('error: ', error);

    })
    
  }
  confirmMinga(){
    Alert.alert('Confirmation','Create Minga?',[
      {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
      {text: 'Create', onPress: () => this.doMinga},
    ]);
  }
  componentWillMount() {
    this.startOver();
  }
  render() {
    var style= require('../utils/Style');
    let pickers = this.state.categories!==null && 
    this.state.categories.map(element=>{
      return(
        <Picker.Item key={element} label={element} value={element} />
      )
    })
    return (
      <Container>
      <Header style={style.normalHeader}>
        <Left style={style.left}>
          <Button transparent onPress={()=>Actions.pop()}>
            <Icon name='ios-arrow-back' />
          </Button>
        </Left>
        <Body style={style.body}>
          <Image source={logo} alt="logo" style={style.logoHeader} resizeMode="contain"/>
        </Body>
        <Right style={style.right} />
      </Header>
      <Content style={style.genericContent}>
        <View style={style.centerWrapper}>
          <Text style={style.br}></Text>
          <Text style={{textAlign:'center'}}>Add Information</Text>
          <Text style={style.br}></Text>
          <Form style={{width:'90%'}}>
          <Item underline style={style.paddingText}>
            <Input placeholder='Title' onChangeText={ (title) => this.setState({ title }) }/>
          </Item>
          <Item underline style={style.paddingText}>
          <Textarea rowSpan={5} placeholder="Description" onChangeText={ (description) => this.setState({ description }) } />
          </Item>
          <DateTimePicker
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this._handleDatePicked}
            onCancel={this._hideDateTimePicker}
          />
          <Text style={style.br}></Text>
          <Item onPress={()=>this.setState({isDateTimePickerVisible:true})}>
          {
            this.state.mingaDate === null ?
            <Text style={{textAlign:'center'}}>Add Date</Text>:
            <Input disabled value={this.state.mingaDate}/>
          }
          </Item>
          
          <Text style={style.br}></Text>

          <Picker
            mode="dropdown"
            iosIcon={<Icon name="ios-arrow-down-outline" />}
            headerStyle={{ backgroundColor: "#00dbdb" }}
            headerBackButtonTextStyle={{ color: "#fff" }}
            headerTitleStyle={{ color: "#fff" }}
            style={{ width: undefined }}
            selectedValue={this.state.selectCategory}
            onValueChange={this.selectCategory}
          >
          {pickers}
          </Picker>
          </Form>
        </View>
          
      </Content>
      {  
        this.state.isLoading ?
        <Spinner size='small' color='black' />
        :
        <Footer>
        <FooterTab>
          <Button block style={style.paddingText} onPress={ this.confirmMinga }>
            <Text style={style.footerText}>Create Minga</Text>
          </Button>
          </FooterTab>
        </Footer>
        }
      </Container>
    );
  }
}