import React from 'react';
import {
  ActivityIndicator,
  Alert,
  Clipboard,
  Image,
  Share,
  StatusBar,
  StyleSheet,
  ActionSheetIOS,
  TouchableOpacity,
  View,
} from 'react-native';
import logo from '../assets/logo.png';
import addImg from '../assets/addImg.png';
import { Container,Drawer, Header, Title, Content, Footer, FooterTab,Button,List,ListItem ,Left, Right, Body, Icon, Text } from 'native-base';
import Exponent, { Constants, ImagePicker, Permissions,registerRootComponent } from 'expo';
import {Actions} from 'react-native-router-flux';
var BUTTONS = [
    { text: "Pick an image from camera roll",icon: "american-football", iconColor: "#2c8ef4"  },
    { text: "Take a photo",icon: "american-football", iconColor: "#2c8ef4" },
    { text: "Option 0", icon: "american-football", iconColor: "#2c8ef4" },
    { text: "Delete", icon: "trash", iconColor: "#fa213b" },
    { text: "Cancel", icon: "close", iconColor: "#25de5b" }
  ];
  var DESTRUCTIVE_INDEX = 3;
  var CANCEL_INDEX = 4;
export default class Demo extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            image: null,
            uploading: false,
            hasImg:false,
          };
    }
  
  componentWillMount() {
      this.getPermissionStatus();
      this.getPermissionCamera();
  }
  getPermissionCamera(){
    const permission = Permissions.CAMERA;
    try {
      Permissions.getAsync(permission).then(perm => {
        this.setState({ status: perm.status });
      });
      Permissions.askAsync(permission);
    } catch (err) {
      return;
    }
  }
  getPermissionStatus() {
    const permission = Permissions.CAMERA_ROLL;
    try {
      Permissions.getAsync(permission).then(perm => {
        this.setState({ status: perm.status });
      });
      Permissions.askAsync(permission);
    } catch (err) {
      return;
    }
  }
  render() {
    let { image } = this.state;
    var style= require('../utils/Style');
    return (
        <Container>
        <Header style={style.normalHeader}>
          <Left style={style.left}>
            <Button transparent  onPress={() => Actions.pop()}>
              <Icon name='ios-arrow-back' />
            </Button>
          </Left>
          <Body style={style.body}>
          <Image source={logo} alt="logo" style={style.logoHeader} resizeMode="contain"/>
          </Body>
          <Right style={style.right} />
        </Header>
        <Content style={style.whiteBg}>

            <View style={style.centerWrapper}>
            <Text style={style.br}></Text>
            <Text style={style.text}>
                Add Image
            </Text>
            <Text style={style.br}></Text>
            {
                this.state.hasImg===false?
                <TouchableOpacity  onPress={() =>
                    ActionSheetIOS.showActionSheetWithOptions({
                        options: ['Camera roll', 'Take a photo','Cancel'],
                        cancelButtonIndex: 2,
                      },
                      (buttonIndex) => {
                        if (buttonIndex === 1) { 
                            this._takePhoto();
                        }
                        if(buttonIndex===0){
                            this._pickImage();
                        }
                      })}>
                <Image source={addImg} style={style.imgAdd}></Image>
                </TouchableOpacity>
                :
                <View>
                {this._maybeRenderImage()}
                {this._maybeRenderUploadingOverlay()}
                <StatusBar barStyle="default" />
                </View>
            }
          </View>
        </Content>
        {
            this.state.hasImg === true &&
            <Footer>
          <FooterTab>
            <Button full onPress={()=>Actions.addNeed()}>
              <Text style={style.footerText}>Siguiente</Text>
            </Button>
          </FooterTab>
        </Footer>
        }
        
      </Container>
     
    );
  }
  confirmImg = () =>{
      let self = this;
      Alert.alert('Confirm','Upload picture?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Upload', onPress: () => Actions.addNeed()},
        ]);
  }
  _maybeRenderUploadingOverlay = () => {
    if (this.state.uploading) {
      return (
        <View
          style={[
            StyleSheet.absoluteFill,
            {
              backgroundColor: 'rgba(0,0,0,0.4)',
              alignItems: 'center',
              justifyContent: 'center',
            },
          ]}>
          <ActivityIndicator color="#fff" animating size="large" />
        </View>
      );
    }
  };

  _maybeRenderImage = () => {
    let { image } = this.state;
    console.log('image: ', image);
    if (!image) {
      return;
    }

    return (
      <View
        style={{
          marginTop: 30,
          width: 250,
          borderRadius: 3,
          elevation: 2,
          shadowColor: 'rgba(0,0,0,1)',
          shadowOpacity: 0.2,
          shadowOffset: { width: 4, height: 4 },
          shadowRadius: 5,
        }}>
        <View
          style={{
            borderTopRightRadius: 3,
            borderTopLeftRadius: 3,
            overflow: 'hidden',
          }}>
          <Image source={{ uri: image }} style={{ width: 250, height: 250 }} />
        </View>
      </View>
    );
  };
  _takePhoto = async () => {
    let pickerResult = await ImagePicker.launchCameraAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });

    this._handleImagePicked(pickerResult);
  };

  _pickImage = async () => {
    let pickerResult = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });

    this._handleImagePicked(pickerResult);
  };

  _handleImagePicked = async pickerResult => {
    let uploadResponse, uploadResult;

    try {
      this.setState({ uploading: true });

      if (!pickerResult.cancelled) {
        uploadResponse = await uploadImageAsync(pickerResult.uri);
        uploadResult = await uploadResponse.json();
        console.log('uploadResult: ', uploadResult);
        this.setState({ image: uploadResult.location ,hasImg:true});
      }
    } catch (e) {
      console.log({ uploadResponse });
      console.log({ uploadResult });
      console.log({ e });
      alert('Upload failed, sorry :(');
    } finally {
      this.setState({ uploading: false });
    }
  };
}

async function uploadImageAsync(uri) {
  let apiUrl = 'https://file-upload-example-backend-dkhqoilqqn.now.sh/upload';

  // Note:
  // Uncomment this if you want to experiment with local server
  //
  // if (Constants.isDevice) {
  //   apiUrl = `https://your-ngrok-subdomain.ngrok.io/upload`;
  // } else {
  //   apiUrl = `http://localhost:3000/upload`
  // }

  let uriParts = uri.split('.');
  let fileType = uriParts[uriParts.length - 1];

  let formData = new FormData();
  formData.append('photo', {
    uri,
    name: `photo.${fileType}`,
    type: `image/${fileType}`,
  });

  let options = {
    method: 'POST',
    body: formData,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
  };

  return fetch(apiUrl, options);
}