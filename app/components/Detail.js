import React from 'react';
import { StyleSheet,  View, Image,Alert } from 'react-native';
import SideBar from './SideBar';
import logo from '../assets/logo.png';
import { Constants, Location, Permissions, } from 'expo';
import { Container,Drawer, Header, Title, Content, Footer, FooterTab,Button,List,ListItem ,Left, Right, Body, Icon, Text,  } from 'native-base';
import {logOut, removeAllItems,getNeeds, getItem} from '../utils/Controller';
import MapView from 'react-native-maps';
import {Actions} from 'react-native-router-flux';
import pin from '../assets/pin.png';

export default class Detail extends React.Component {
    constructor(props){
        super(props);
        this.state={
            location:null,
            needs: null,
            city:'Quito',
            neightborhood:'',
        }
    }
    
      _getLocationAsync = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
          this.setState({
            error: 'Permission to access location was denied',
          });
        }
        let location = await Location.getCurrentPositionAsync({});
        let currentLocation = [location.coords.latitude, location.coords.longitude];
        this.setState({ location:currentLocation });
      };
      

      startOver(){  
        this._getLocationAsync();
      }
      componentWillMount() {
          this.startOver();
      }
      confirmJoin = () =>{
        Alert.alert('Confirmation','Join this Minga?',
            [
            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'Join', onPress: () => Actions.index()},
            ]
        );
      }
  render() {
    var style= require('../utils/Style');
    let myDate = new Date(this.props.item.fechaMinga);
    let fechaMinga = myDate.getFullYear() + '-' +('0' + (myDate.getMonth()+1)).slice(-2)+ '-' +  myDate.getDate() + ' a las '+myDate.getHours()+ ':'+('0' + (myDate.getMinutes())).slice(-2);
    return (

        <Container>
        <Header style={style.normalHeader}>
          <Left style={style.left}>
            <Button transparent  onPress={() => Actions.pop()}>
              <Icon name='ios-arrow-back' />
            </Button>
          </Left>
          <Body style={style.body}>
          <Image source={logo} alt="logo" style={style.logoHeader} resizeMode="contain"/>
          </Body>
          <Right style={style.right} />
        </Header>
        <Content style={style.whiteBg}>
          {
              this.state.location!==null ?
              <View>
              {
                this.state.location.length===2 ?
                <MapView
                    style={style.mapFinder}
                    showsUserLocation={true}
                    zoomEnabled = {true}
                    showsMyLocationButton={true}
                    scrollEnabled = {true}
                    initialRegion={{
                        latitude: parseFloat(this.state.location[0]),
                        longitude: parseFloat(this.state.location[1]),
                        latitudeDelta: 0.0143,
                        longitudeDelta:  0.0143,
                    }}
                    >
                        <MapView.Marker
                        coordinate={{latitude: parseFloat(this.props.item.Location.split(',')[0]),longitude: parseFloat(this.props.item.Location.split(',')[1])}}
                        title={this.props.item.Nombre}
                        description={this.props.item.Description}
                    >
                    <View>
                      <Image source={pin} style={style.pin} resizeMode="contain"></Image>
                    </View>
                    <MapView.Callout onPress={() => Actions.info({store:element})} >
                    <View style={style.viewPin}>
                      <Text>{this.props.item.Título}</Text>
                    </View>
                    </MapView.Callout>
                  </MapView.Marker>
                </MapView>
                :
                <View style={style.centerWrapper}>
                <Text style={style.center}>Buscando Localización...</Text>
                </View>
              }
            
          </View>
          :
          <View style={style.centerWrapper}>
                    <Text style={style.center}>Buscando Localización...</Text>
                </View>
          }
          {
              this.props.item!==null &&
              <List>
                    <ListItem>
                        <Image source={{uri:this.props.item.imgUrl}} style={style.miniImg} alt="Image" resizeMode="contain" />
                        <Body style={style.body}>
                            <Text style={style.titleList}>{this.props.item.Título}</Text>
                            <Text style={style.subTitleList}>{this.props.item.Descripción}</Text>
                            <Text style={style.subTitleList}>{fechaMinga}</Text>
                            <Text style={style.subTitleListJoin}>Join (+10 pts)</Text>
                        </Body>
                    </ListItem>
              </List>
              
          }
        </Content>
        <Footer>
          <FooterTab>
            <Button full onPress={this.confirmJoin}>
              <Text style={style.footerText}>Join</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}