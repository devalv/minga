import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text ,Picker,Form} from 'native-base';
import logo from '../assets/logo.png';
import { Constants, Location, Permissions, } from 'expo';
import {Actions} from 'react-native-router-flux';
import {setItem,getItem} from '../utils/Controller';

export default class First extends React.Component {
  constructor(props){
    super(props);
    this.state={
      selectNeightborhood: "Miraflores",
      location:null,
      neightbors:['Miraflores','Iñaquito','Ponciano Alto','La Primavera','Guamani']
    };
  }
  selectNeightborhood = async (value) =>{
    console.log('value: ', value);
    await setItem('neightborhood',value);
    Actions.login();
    this.setState({
      selectNeightborhood: value
    });
  }
  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        error: 'Permission to access location was denied',
      });
    }
    let location = await Location.getCurrentPositionAsync({});
    let currentLocation = [location.coords.latitude, location.coords.longitude];
    this.setState({ location:currentLocation });
  };
  startOver(){
    this._getLocationAsync();
  }
  componentWillMount() {
    this.startOver();
  }
  render() {
    var style= require('../utils/Style');
    let pickers = this.state.neightbors!==null && 
    this.state.neightbors.map(element=>{
      return(
        <Picker.Item key={element} label={element} value={element} />
      )
    })
    return (
      <Container style={style.firstContainer}>
        <Image source={logo} alt="logo" style={style.firstLogo} resizeMode="contain"></Image>
        <View style={style.displayInlineBlock}>
          <Text style={style.firstText}>Choose your </Text><Text style={style.firstYellow}> neightborhood</Text>
        </View>
        <Form>
          <Picker
            mode="dropdown"
            iosIcon={<Icon name="ios-arrow-down-outline" />}
            headerStyle={{ backgroundColor: "#00dbdb" }}
            headerBackButtonTextStyle={{ color: "#fff" }}
            headerTitleStyle={{ color: "#fff" }}
            style={{ width: undefined }}
            selectedValue={this.state.selectNeightborhood}
            onValueChange={this.selectNeightborhood}
          >
          {pickers}
          </Picker>
        </Form>
      </Container>
    );
  }
}