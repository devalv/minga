import React from 'react';
import { StyleSheet,  View, Image } from 'react-native';
import SideBar from './SideBar';
import logo from '../assets/logo.png';
import { Constants, Location, Permissions, } from 'expo';
import { Container,Drawer, Header, Title, Content, Footer, FooterTab,Button,List,ListItem ,Left, Right, Body, Icon, Text, } from 'native-base';
import {logOut, removeAllItems,getNeeds, getItem} from '../utils/Controller';
import MapView from 'react-native-maps';
import {Actions} from 'react-native-router-flux';
import pin from '../assets/pin.png';

export default class Index extends React.Component {
    constructor(props){
        super(props);
        this.state={
            location:null,
            needs: null,
            city:'Quito',
            neightborhood:'',
        }
    }
    closeDrawer = () => {
        this.drawer._root.close()
      };
      openDrawer = () => {
        this.drawer._root.open()
      };
      logOut = () =>{
        logout().then(value=>{
            removeAllItems();
            Actions.first();
        })
        .catch(error=>{
            console.log('error: ', error);
    
        })
      }
      AddNeed = () =>{
          Actions.demo()
      }
      _getLocationAsync = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
          this.setState({
            error: 'Permission to access location was denied',
          });
        }
        let location = await Location.getCurrentPositionAsync({});
        let currentLocation = [location.coords.latitude, location.coords.longitude];
        this.setState({ location:currentLocation });
      };
      async getNeedsFunction(){
        let self = this;
        let neightborhood = await getItem('neightborhood');
        if(neightborhood!==null){
          self.setState({neightborhood});
          getNeeds('Quito',neightborhood).then(value=>{
            let needs = value.toJSON();
            self.setState({needs})
          }).catch(error=>{
            console.log('error: ', error);
          })
        }
      }

      startOver(){  
        this._getLocationAsync();
        this.getNeedsFunction();
      }
      componentWillMount() {
          this.startOver();
      }
  render() {
    var style= require('../utils/Style');
    var markers = this.state.needs!==null &&
    Object.values(this.state.needs).map((element,index)=>{
      console.log('element: ', element);
      let coordenadas = element.Location.split(',');
      return(
        <MapView.Marker
                key = {index}
                coordinate={{latitude: parseFloat(coordenadas[0]),longitude: parseFloat(coordenadas[1])}}
                title={element.Nombre}
                description={element.Description}
              >
                <View>
                  <Image source={pin} style={style.pin} resizeMode="contain"></Image>
                </View>
                <MapView.Callout onPress={() => Actions.info({store:element})} >
                <View style={style.viewPin}>
                  <Text>{element.Título}</Text>
                </View>
                </MapView.Callout>
              </MapView.Marker>
        );
    })
    return (
        <Drawer
        ref={(ref) => { this.drawer = ref; }}
        content={<SideBar></SideBar>  }
        onClose={() => this.closeDrawer()} >
        <Container>
        <Header style={style.normalHeader}>
          <Left style={style.left}>
            <Button transparent  onPress={() => this.openDrawer()}>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body style={style.body}>
          <Image source={logo} alt="logo" style={style.logoHeader} resizeMode="contain"/>
          </Body>
          <Right style={style.right} />
        </Header>
        <Content style={style.whiteBg}>
          <View style={[style.displayInlineBlock,style.customHeader]}>
            <Text style={style.customHeaderText}>{this.state.neightborhood.toUpperCase()} </Text><Text style={style.miniHeaderText}> Quito, Ecuador</Text>
          </View>
          {
              this.state.location!==null ?
              <View>
              {
                this.state.location.length===2 ?
                <MapView
                    style={style.mapFinder}
                    showsUserLocation={true}
                    zoomEnabled = {true}
                    showsMyLocationButton={true}
                    scrollEnabled = {true}
                    initialRegion={{
                        latitude: parseFloat(this.state.location[0]),
                        longitude: parseFloat(this.state.location[1]),
                        latitudeDelta: 0.0143,
                        longitudeDelta:  0.0143,
                    }}
                    >
                  {markers}
                </MapView>
                :
                <View style={style.centerWrapper}>
                  <Text style={style.center}>Buscando Localización...</Text>
                </View>
              }
            
          </View>
          :
          <View style={style.centerWrapper}>
            <Text style={style.center}>Buscando Localización...</Text>
          </View>
          }
          {
            this.state.needs!== null &&
            <List
              dataArray={Object.values(this.state.needs)}
              renderRow={(item) =>{
                var myDate = new Date(item.fechaMinga);
                /*month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();
                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;
                let fechaMinga= [year, month, day].join('-');*/
                let fechaMinga = myDate.getFullYear() + '-' +('0' + (myDate.getMonth()+1)).slice(-2)+ '-' +  myDate.getDate() + ' a las '+myDate.getHours()+ ':'+('0' + (myDate.getMinutes())).slice(-2);
                return(
                <ListItem onPress={()=>Actions.detail({item})}>
                  <Image style={style.miniImg} source={{uri:item.imgUrl}} resizeMode="contain"></Image>
                  <Body style={style.body}>
                    <Text style={style.titleList}>{item.Título}</Text>
                    <Text style={style.subTitleList}>{item.Descripción}</Text>
                    <Text style={style.subTitleList}>{fechaMinga}</Text>
                    <Text style={style.subTitleListJoin}>Join (+10 pts)</Text>
                  </Body>                  
                </ListItem>
                );
              } 
            }/>
          }
        </Content>
        <Footer>
          <FooterTab>
            <Button full onPress={this.AddNeed}>
              <Text style={style.footerText}>Agregar Necesidad</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
      </Drawer>
    );
  }
}