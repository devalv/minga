import React from 'react';
import { StyleSheet,  View ,Image, Alert} from 'react-native';
import { Container, Header, Title, Content, Footer,Spinner, FooterTab, Form,Item,Input,Button, Left, Right, Body, Icon, Text } from 'native-base';
import logo from '../assets/logo.png';
import platforms from '../assets/platforms.png';
import {logIn,setItem} from '../utils/Controller';
import {Actions} from 'react-native-router-flux';

export default class Login extends React.Component {
    constructor(props){
        super(props);
        this.state={
            isLoadding:false,
            mail:'',
            password:'',
        };
    }
    onLoginButtonPress = () =>{
        let mail = this.state.mail;
        let pass = this.state.password;
        let self = this;
        self.setState({isLoading:true})
        logIn(mail,pass).then(async (value)=>{
            console.log('value: ', value);
            await setItem('uid',value.user.uid);
            self.setState({isLoading:false})
            Actions.index();
        }).catch(error=>{
            Alert.alert('Error','Datos Incorrectos');
            self.setState({isLoading:false})
            console.log('error: ', error);
        })
    }
  render() {
    var style= require('../utils/Style');
    return (
        <Container>
        <Header style={style.bigHeader}>
          <Left style={style.left}>
          </Left>
          <Body style={style.body}>
            <Image source={logo} alt="logo" style={style.logoHeader} resizeMode="contain"/>
          </Body>
          <Right style={style.right} />
        </Header>
        <Content style={style.genericContent}>
        <View style={style.centerWrapper}>
            <Text style={style.br}></Text>
            <Text style={style.text}>
                Log in with
            </Text>
            <Image style={style.loginPlatforms} source={platforms} resizeMode='contain'></Image>
            <Text style={style.text}>
                or
            </Text>
            <Text style={style.br}></Text>
            <Form style={{width:'70%',}}>
                <Item underline style={style.paddingText}>
                    <Input autoCorrect={ false }   autoCapitalize = 'none' placeholder='Mail' onChangeText={ (mail) => this.setState({ mail }) }/>
                </Item>
                <Item underline style={style.paddingText}>
                    <Input placeholder='Password' onChangeText={ (password) => this.setState({ password }) } secureTextEntry />
                </Item>
                <Text style={style.br}></Text>
                    {  
                    this.state.isLoading ?
                    <Spinner size='small' color='black' />
                    :
                    <View>
                        <Button block  style={style.paddingText} onPress={ this.onLoginButtonPress }>
                        <Text>Iniciar Sesión</Text>
                        </Button>
                    </View>
                    }
                    

                <Text style={style.br}></Text>
            <Text style={[style.text,style.textAlignCenter]}>
                Doesn't have an account?
            </Text>
            <Text style={style.br}></Text>
            <Button block style={style.paddingText}>
                <Text>Sign up</Text>
            </Button>
            </Form>
            
        </View>
          
        </Content>
      </Container>
    );
  }
}