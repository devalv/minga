import React from 'react';
import { StyleSheet, View, Image , Alert} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text ,Picker,Form} from 'native-base';
import logo from '../assets/logo.png';
import {Actions} from 'react-native-router-flux';

import {setItem,getItem,removeAllItems,logout} from '../utils/Controller';

export default class SideBar extends React.Component {
  constructor(props){
    super(props);
    this.state={

    };
  }
  logOut=()=>{
    logout().then(value=>{
        console.log('value: ', value);
        removeAllItems();
        Actions.first();
    })
    .catch(error=>{
        Alert.alert('Error','Revise su conexión')
        console.log('error: ', error);

    })
  }
  render() {
    var style= require('../utils/Style');
    return (
      <View style={style.SideBar}>
        <Button block style={style.drawerButton} onPress={this.logOut}>
        <Text>Log Out</Text>
        </Button>
      </View>
    );
  }
}