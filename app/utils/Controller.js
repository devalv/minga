import firebase from './Firebase';
import { Permissions, Notifications } from 'expo';
import {  AsyncStorage } from 'react-native';
export let db = firebase.database();
export let auth = firebase.auth();

export function getCurrentUser() {
    return auth.currentUser;
}
export function getNeeds(city,neightborhood){
    let ref=db.ref('ciudades/'+city+'/'+neightborhood+'/Problemas');
    return ref.once('value',snapshot=>{
      return snapshot.toJSON(); 
    });
  }
export function uploadMinga (data){
    let ref = db.ref('ciudades/Quito/Iñaquito/Problemas').push(data);
    return true;
}
export function logout() {
    return auth.signOut();
}
export async function removeAllItems() {
    await AsyncStorage.clear();
}
export function logIn(email, password) {
    return auth.signInWithEmailAndPassword(email, password);
}
  export async function getItem(item) {
    try {
      const value = await AsyncStorage.getItem(item);
      return value;
    } catch (error) {
        console.log('error getting item: ', error);
        return null;
    }
    return null;
}
export async function setItem(name,item) {
    try {
      const value = await AsyncStorage.setItem(name,item);
      return value;
    } catch (error) {
        console.log('error setting item: ', error);
        return null;
    }
    return null;
}
