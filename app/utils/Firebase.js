import firebase from 'firebase';
export default firebase.initializeApp({
    apiKey: "AIzaSyC7UNT4Gjyv-2XqCviE4-fv1TQ0jB-WiT0",
    authDomain: "minga-app.firebaseapp.com",
    databaseURL: "https://minga-app.firebaseio.com",
    projectId: "minga-app",
    storageBucket: "minga-app.appspot.com",
    messagingSenderId: "88447792390"
  });
