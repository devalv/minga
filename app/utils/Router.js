import React,{Component} from 'react';
import { Platform } from 'react-native';
import { Router,Stack, Scene, Actions } from 'react-native-router-flux';
import firebase from 'firebase';
import {Button, Text, Icon } from 'native-base';
import Login from '../components/Login';
import Index from '../components/Index';
import First from '../components/First';
import AddNeed from '../components/AddNeed';
import Detail from '../components/Detail';
import Demo from '../components/Demo';


export default class RouterComponent extends Component{
render(){
    return (
      <Router>
        <Stack key="root"  panHandlers={null}>
          <Scene key="first" component={First} hideNavBar={true} initial={this.props.neightborhood?false:true}/>
          <Scene key="login" component={Login} hideNavBar={true} initial={this.props.isLoggedIn?false:true}/>
          <Scene key="index" component={Index} hideNavBar={true} initial={this.props.isLoggedIn?true:false}/>
          <Scene key="addNeed" component={AddNeed} hideNavBar={true} />
          <Scene key="detail" component={Detail} hideNavBar={true} />
          <Scene key="demo" component={Demo} hideNavBar={true} />

          
        </Stack>
      </Router>
    );
  }
}
